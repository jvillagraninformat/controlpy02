from django.forms import ModelForm, TextInput
from tickets.models import *

class FormTickets(ModelForm):
    class Meta:
        model = Tickets
        fields = '__all__'
        widgets = {
            'titulo': TextInput(attrs={'type': 'text'}),
            'descripcion': TextInput(attrs={'type': 'text'})
        }
