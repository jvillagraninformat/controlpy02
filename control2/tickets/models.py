from django.db import models
from datetime import datetime


# Create your models here.
class Tickets(models.Model):
    titulo = models.CharField(max_length=50)
    descripcion = models.CharField(max_length=100)
    ESTADOS_TICKETS = [
        ('0', 'Abierto'),
        ('1', 'Pendiente'),
        ('2', 'En Proceso'),
        ('3', 'Resuelto'),
        ('4', 'Cerrado'),
    ]
    estado = models.CharField(max_length=2, choices=ESTADOS_TICKETS, default='0')
    fecha_creacion = models.DateTimeField(default=datetime.now, verbose_name='Fecha Creacion')


    def __str__(self):
        txt = 'Titulo:{0} - {1} (Estado: {2} )'
        return txt.format(self.titulo, self.descripcion, self.get_estado_display())
        #return f'Ticket {self.id}: {self.titulo} - {self.descripcion} - {self.get_estado_display()} {self.fecha_creacion}'

