from django.contrib import admin
from django.contrib.admin import SimpleListFilter

# Register your models here.
from tickets.models import Tickets

class TicketsAdmin(admin.ModelAdmin):
    list_display = ("id", "titulo", "descripcion", "estado", "fecha_creacion")
    search_fields = ("titulo", "descripcion", "estado", "fecha_creacion")
    list_filter = ("titulo", "descripcion", "estado", "fecha_creacion")

admin.site.register(Tickets, TicketsAdmin)