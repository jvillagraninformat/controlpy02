from django.shortcuts import render, redirect

# Create your views here.
from tickets.forms import FormTickets
from tickets.models import Tickets

def tickets_list(request):
    datos = {
        'title': 'Listado Tickets',
        'tickets': Tickets.objects.all()
    }
    return render(request,'listado.html', datos)

# def listadoTickets(request):
#     no_tickets = Tickets.objects.count()
#     tickets = Tickets.objects.order_by('id')
#     parametros = {'no_tickets': no_tickets, 'tickets': tickets}
#     return render(request, 'lista.html', parametros)

def nuevoTickets(request):
    if request.method == 'POST':
        formaTickets = FormTickets(request.POST)
        if formaTickets.is_valid():
            formaTickets.save()
            return redirect('TicketsLista')
    else:
        formaTickets = FormTickets()
    return render(request,'nuevo.html', {'formaTickets': formaTickets})